using UnityEngine;

namespace Common
{
    public static class ExtensionMethods
    {
        public static void NormalizeToY(this Transform transform)
        {
            var euler = transform.eulerAngles;
            euler.z = 0f;
            euler.x = 0f;
            transform.eulerAngles = euler;
        }
    }
}