using UnityEngine;

namespace Common
{
    public class GradientEffect : MonoBehaviour
    {
        public SpriteRenderer TargetSprite;
        public Gradient Gradient;

        public void SetColor(float value) => TargetSprite.color = Gradient.Evaluate(value);
    }
}