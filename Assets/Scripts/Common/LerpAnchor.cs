using UnityEngine;

namespace Common
{
    public class LerpAnchor : MonoBehaviour
    {
        public Transform Anchor;
        public float Factor;

        void Update() => transform.position = Vector3.Lerp(transform.position, Anchor.position, Factor);
    }
}