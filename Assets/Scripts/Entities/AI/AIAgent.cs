using System.Collections;
using Common;
using Entities.AI.Patrol;
using UnityEngine;
using UnityEngine.AI;

namespace Entities.AI
{
    public class AIAgent : MonoBehaviour
    {
        public PatrolPath Patrol;
        public Navigator Navigator;
        public NavMeshAgent Agent;
        public float PatrolWait;
        public Awareness Awareness;
        public CharacterAnimation Animator;

        Transform currentPathNode;
        bool isForward;
        
        void Start()
        {
            Navigator.Initialize(Agent, Animator);
            Awareness.Initialize(this, Animator);
            Animator.SetWeaponHands(2);
            
            currentPathNode = Patrol.GetFirstNode();
            
            Navigator.GoToPoint(currentPathNode.position, WaitAndGoToNext);
        }

        void WaitAndGoToNext()
        {
            SwitchNode();
            StartCoroutine(WaitThenContinue());
        }
        
        void WaitAndResumePatrol() => StartCoroutine(WaitThenContinue());

        IEnumerator WaitThenContinue()
        {
            yield return new WaitForSeconds(PatrolWait);
            WalkToTarget();
        }

        void SwitchNode()
        {
            if (Patrol.IsEndNode(currentPathNode))
                isForward = !isForward;
            
            currentPathNode = Patrol.GetNextNode(currentPathNode, isForward);
        }
        
        void WalkToTarget() => Navigator.GoToPoint(currentPathNode.position, WaitAndGoToNext);

        public void TargetInSight(Vector3 targetPosition)
        {
            StopAllCoroutines();
            Navigator.StopMoving();
            transform.LookAt(targetPosition);
            transform.NormalizeToY();
        }
        
        public void TargetNoLongerInSight(Vector3 lastPosition)
        {
            StopAllCoroutines();
            Navigator.GoToPoint(lastPosition, WaitAndResumePatrol);
        }
    }
}