using Common;
using UnityEngine;

namespace Entities.AI
{
    public class Awareness : MonoBehaviour
    {
        public VisibilityCone Visibility;
        public Transform Target;
        public ParticleSystem AwareVfx;
        public GradientEffect Meter;
        public float AwarenessRaiseFactor;
        public float AwarenessDecreaseFactor;

        bool wasInSight;
        float awareness;
        AIAgent aiAgent;
        CharacterAnimation animator;

        bool TargetInSight => Visibility.IsInSight(Target.position);
        
        void Update()
        {
            if (TargetInSight)
            {
                if (awareness < 1)
                    awareness += AwarenessRaiseFactor/Vector3.Distance(transform.position, Target.position);
                else
                {
                    aiAgent.TargetInSight(Target.position);
                    if (!AwareVfx.isPlaying)
                        AwareVfx.Play();
                }

                if(!wasInSight)
                    animator.StartAiming();
            }
            else
            {
                if (awareness >= 1)
                {
                    aiAgent.TargetNoLongerInSight(Target.position);
                    animator.StopAiming();
                }

                if(awareness > 0)
                    awareness -= AwarenessDecreaseFactor;
            }

            wasInSight = TargetInSight;
            Meter.SetColor(awareness);
        }

        public void Initialize(AIAgent aiAgent, CharacterAnimation animator)
        {
            this.aiAgent = aiAgent;
            this.animator = animator;
        }
    }
}