using System;
using UnityEngine;
using UnityEngine.AI;

namespace Entities.AI
{
    public class Navigator : MonoBehaviour
    {
        public float ArrivalThreshold;

        CharacterAnimation animator;
        NavMeshAgent agent;
        Action onArrive;
        Vector3 destination;
        bool hasDestinationTrigger;
        
        public void Initialize(NavMeshAgent agent, CharacterAnimation animator)
        {
            this.agent = agent;
            this.animator = animator;
        }

        void StartMovingTowards(Vector3 target)
        {
            destination = target;
            agent.destination = target;
            agent.isStopped = false;
            animator.SetMoveSpeed(agent.speed);
        }
        
        public void GoToPoint(Vector3 target)
        {
            StartMovingTowards(target);
            hasDestinationTrigger = false;
        }
        
        public void GoToPoint(Vector3 target, Action doOnArrive)
        {
            onArrive = doOnArrive;
            
            StartMovingTowards(target);
            hasDestinationTrigger = true;
        }

        void Update()
        {
            if (!agent.isStopped && hasDestinationTrigger && Vector3.Distance(destination, transform.position) < ArrivalThreshold)
            {
                onArrive();
                agent.isStopped = true;
                animator.SetMoveSpeed(0);
            }
        }

        public void StopMoving()
        {
            agent.SetDestination(transform.position);
            agent.isStopped = true;
            animator.SetMoveSpeed(0);
        }
    }
}