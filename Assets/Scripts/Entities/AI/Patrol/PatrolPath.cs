﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Entities.AI.Patrol
{
    public class PatrolPath : MonoBehaviour
    {
        public GameObject NodePrefab;

        List<Transform> nodes;

        void Awake() => nodes = GetComponentsInChildren<Transform>().Where(t => !t.Equals(transform)).ToList();

        public Transform GetFirstNode() => nodes.First();

        public Transform GetNextNode(Transform start, bool isForward)
        {
            var index = nodes.IndexOf(start);
            return isForward ? nodes[index + 1] : nodes[index - 1];
        }

        public bool IsEndNode(Transform node) => node == nodes.Last() || node == nodes.First();

        void OnDrawGizmos()
        {
            nodes = GetComponentsInChildren<Transform>().Where(t => !t.Equals(transform)).ToList();
            if (nodes.Count > 1)
                foreach (var i in Enumerable.Range(0, nodes.Count - 1))
                    Debug.DrawLine(nodes[i].position, nodes[i + 1].position, Color.red);
        }

        public void AddNode()
        {
            var node = Instantiate(NodePrefab, transform).transform;
            nodes.Add(node);
        }
    }
}
