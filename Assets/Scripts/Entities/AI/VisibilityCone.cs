﻿using UnityEngine;

namespace Entities.AI
{
    public class VisibilityCone : MonoBehaviour
    {
        public float Angle;
        public float Range;
        public LayerMask ObstacleLayer;

        float width => Angle / 45;

        void OnDrawGizmos()
        {
            var sightLimitRight = new Vector3(width / 2, 0, 1) * Range;
            var sightLimitLeft = new Vector3(-width / 2, 0, 1) * Range;

            Debug.DrawLine(transform.position, transform.TransformPoint(sightLimitLeft), Color.yellow);
            Debug.DrawLine(transform.position, transform.TransformPoint(sightLimitRight), Color.yellow);
        }

        bool HasLineOfSight(Vector3 targetPosition)
        {
            var pos = transform.position;
            var distance = Vector3.Distance(pos, targetPosition);
            Debug.DrawRay(pos, targetPosition - pos, Color.red);
            return !Physics.Raycast(pos, targetPosition - pos,
                distance,
                ObstacleLayer);
        }

        public bool IsInSight(Vector3 target)
        {
            var targetRelative = transform.InverseTransformPoint(target);
            var inRange = Vector3.Distance(transform.position, target) < Range;
            var inArcRight = targetRelative.x + (width/2) * targetRelative.z > 0;
            var inArcLeft = targetRelative.x - (width/2) * targetRelative.z < 0;

            var isInArc = inArcRight & inArcLeft;
            return inRange && isInArc && HasLineOfSight(target);
        }
    }
}
