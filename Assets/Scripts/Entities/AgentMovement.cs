﻿using UnityEngine;
using UnityEngine.AI;

namespace Entities
{
    public class AgentMovement : MonoBehaviour
    {
        public NavMeshAgent Agent;
        public CharacterAnimation Animator;
        public float Speed;

        void Start() => Animator.SetWeaponHands(1);

        public void Move(Vector3 moveVector)
        {
            var normalizedMovement = moveVector.normalized;
            MoveTo(normalizedMovement * Speed * Time.deltaTime);
            Animator.SetMoveSpeed(Speed);

            if (moveVector.magnitude == 0f)
                Animator.SetMoveSpeed(0);
        }

        public void MoveTo(Vector3 destination)
        {
            Agent.isStopped = false;
            Agent.SetDestination(transform.position + destination);
            Animator.SetMoveSpeed(Speed);
        }

        public void Stop() => Agent.isStopped = true;
    }
}
