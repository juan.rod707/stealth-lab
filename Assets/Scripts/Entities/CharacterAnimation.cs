﻿using UnityEngine;

namespace Entities
{
    public class CharacterAnimation : MonoBehaviour
    {
        public Animator Animator;

        public void StartAiming() => Animator.SetTrigger("Aim");

        public void StopAiming() => Animator.SetTrigger("StopAim");

        public void TriggerDeath() => Animator.SetTrigger("Die");

        public void SetMoveSpeed(float moveSpeed) => Animator.SetFloat("MoveSpeed", moveSpeed / 5);

        public void SetWeaponHands(int hands) => Animator.SetInteger("WeaponHands", hands);
    }
}
