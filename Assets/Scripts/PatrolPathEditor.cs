﻿using Entities.AI.Patrol;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PatrolPath))]
public class PatrolPathEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PatrolPath patrolScript = (PatrolPath)target;
        if (GUILayout.Button("Build Object"))
        {
            patrolScript.AddNode();
        }
    }
}