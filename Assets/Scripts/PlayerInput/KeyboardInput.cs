﻿
using Entities;
using UnityEngine;

namespace PlayerInput
{
    public class KeyboardInput : MonoBehaviour
    {
        public AgentMovement ControlledMovement;

        void FixedUpdate()
        {
            var moveVector = new Vector3(
                Input.GetAxisRaw("Horizontal"),
                0f,
                Input.GetAxisRaw("Vertical")
            );

            ControlledMovement.Move(moveVector);
        }
    }
}
